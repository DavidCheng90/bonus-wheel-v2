#define COCOS2D_DEBUG 1

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	pressedButton = false;

	//////////////////////////////
	// 2. get original background
	bgOriginal = Sprite::create("Circus.png");

	auto bgWidth = bgOriginal->getContentSize().width;
	auto bgHeight = bgOriginal->getContentSize().height;

	bgOriginal->setFlippedY(true);
	bgOriginal->setScale(visibleSize.width / bgWidth, visibleSize.height / bgHeight);
	bgOriginal->setPosition(visibleSize.width / 2, visibleSize.height / 2);

	//////////////////////////////
	// 3. create new texture with original background but use the exact size of the screen
	auto bgChangeSize = RenderTexture::create(visibleSize.width, visibleSize.height);

	bgChangeSize->begin();
	bgOriginal->visit();
	bgChangeSize->end();

	//////////////////////////////
	// 4. set the newly resized background
	bgResized = Sprite::createWithTexture(bgChangeSize->getSprite()->getTexture());

	bgResized->setPosition(Point((visibleSize.width / 2) + origin.x, (visibleSize.height / 2) + origin.y));
	this->addChild(bgResized);

	//////////////////////////////
	// 5. set wheel
	assetWheel = Sprite::create("wheel_sections_8.png");

	assetWheel->setPosition(visibleSize.width / 2, visibleSize.height / 1.7);
	assetWheel->setScale(0.28);
	this->addChild(assetWheel);

	auto wheelPosition = assetWheel->getPosition(); // get position of wheel

	//////////////////////////////
	// 6. set prizes
	bonusWheel.addPrizes(assetWheel);

	//////////////////////////////
	// 7. set wheel border
	assetWheelBorder = Sprite::create("wheel_border.png");

	assetWheelBorder->setPosition(wheelPosition);
	assetWheelBorder->setScale(0.28);
	this->addChild(assetWheelBorder);

	//////////////////////////////
	// 8. set wheel arrow
	assetArrow = Sprite::create("wheel_arrow.png");

	assetArrow->setPosition(wheelPosition.x, wheelPosition.y + 128);
	assetArrow->setScale(0.28);
	this->addChild(assetArrow);

	//////////////////////////////
	// 9. set spin button
	assetButton = ui::Button::create("spin_button.png");

	assetButton->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 6.5));
	assetButton->setScale(0.28);
	this->addChild(assetButton);

	assetButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld::pressButton, this)); // event listener for spin button

	buttonText = ui::Text::create("Spin!", "Roboto-Black.ttf", 130); // spin button text

	buttonText->setPosition(Vec2(309, 137));
	buttonText->setTextColor(Color4B::WHITE);
	buttonText->enableOutline(Color4B(0, 210, 50, 255), 8);
	assetButton->addChild(buttonText);
    
    return true;
}

void HelloWorld::pressButton(Ref *sender, ui::Widget::TouchEventType type)
{
	if (!pressedButton) // once user spins the wheel, user cannot spin again
	{
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN: // user's finger is on the spin button, wind back the wheel a little bit
		{
			auto setWheel = RotateBy::create(1, -30);
			assetWheel->runAction(setWheel);
			break;
		}
		case ui::Widget::TouchEventType::ENDED: // user's finger let go of the spin button, spin the wheel
		{
			if (pressedButton = true)
			{
				bonusWheel.spinWheel();

				// ***** function for manually checking each sector of the wheel *****
				// how to use: Replace the "0" parameter with the number of the sector you wish to test
				// 1 - "Life 30 mins."
				// 2 - "Brush 3x"
				// 3 - "Gems 35"
				// 4 - Hammer 3x"
				// 5 - "Coins 750"
				// 6 - "Brush 1x"
				// 7 - "Gems 75"
				// 8 - "Hammer 1x"
				bonusWheel.testWheelSectorHere(0);
				// ********************************************************************

				bonusWheel.wheelAnimation(assetWheel, assetButton, buttonText);

				CCLOG("\nWow! You've won \"%s\"!!!\n\nBonus Wheel 1000x Spins Results:\n1.) Life 30 mins.: %i\n2.) Brush 3x: %i\n3.) Gems 35: %i\n4.) Hammer 3x: %i\n5.) Coins 750: %i\n6.) Brush 1x: %i\n7.) Gems 75: %i\n8.) Hammer 1x: %i\n",
					bonusWheel.prizeList[bonusWheel.yourPrizeNum].getPrizeDescription().c_str(),
					bonusWheel.prizeCount[0],
					bonusWheel.prizeCount[1],
					bonusWheel.prizeCount[2],
					bonusWheel.prizeCount[3],
					bonusWheel.prizeCount[4],
					bonusWheel.prizeCount[5],
					bonusWheel.prizeCount[6],
					bonusWheel.prizeCount[7]);
			}
			break;
		}
		case ui::Widget::TouchEventType::CANCELED:
		{
			auto resetWheel = RotateTo::create(0, 0);
			assetWheel->runAction(resetWheel);
			break;
		}
		default:
			break;
		}
	}
}