#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "proj.win32/Wheel.h"

USING_NS_CC;

USING_NS_CC_EXT;
using namespace ui;

class HelloWorld : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

	// original background
	cocos2d::Sprite *bgOriginal;

	// resized background
	cocos2d::Sprite *bgResized;

	// asset sprites
	cocos2d::Sprite* assetWheel;
	cocos2d::Sprite* assetWheelBorder;
	cocos2d::Sprite* assetArrow;

	ui::Button* assetButton;
	ui::Text* buttonText;

	// method for pressing the spin button
	void pressButton(Ref *sender, ui::Widget::TouchEventType type);

private:
	Wheel bonusWheel;
	bool pressedButton;
};

#endif // __HELLOWORLD_SCENE_H__
