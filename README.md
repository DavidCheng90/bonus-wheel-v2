# Bonus Wheel (Version 2.0)
Bonus wheel game (updated)

## Updates:
1.) Broke down code into 3 classes: Item, Wheel, and HelloWorldScene (Main Scene)

2.) Removed copy/paste code. Replaced with scalable code.

3.) Simulation for 1000 spins now runs in the same function used to reward prize to player

4.) Cleaned up loop condition logics