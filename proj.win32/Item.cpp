#include "Item.h"

Item::Item()
{
	prizeDescription = "0";;
	prizeDropRate = 0;
	prizeRotationDegrees = 0.0;
}

Item::Item(cocos2d::Sprite* name, std::string description, std::string imagePNG, int posX, int posY,
		   double scaleBy, ui::Text* textName, std::string textDescription, int dropRatePercentage, cocos2d::Sprite* parent)
{
	// create prize and set properties
	name = Sprite::create(imagePNG);
	prizeSprite = name;
	prizeDescription = description;
	name->setPosition(posX, posY);
	name->setScale(scaleBy);
	prizeDropRate = dropRatePercentage;

	// draw prize onto wheel
	parent->addChild(name);

	// create prize text
	textName = ui::Text::create(textDescription, "Roboto-Black.ttf", 70 / scaleBy);
	prizeText = textName;
	textName->setPosition(Vec2(name->getContentSize().width - textName->getContentSize().width / 2, textName->getContentSize().height / 2));
	textName->setTextColor(Color4B::WHITE);
	textName->enableOutline(Color4B(141, 94, 47, 255), 4);
	name->addChild(textName);
}

cocos2d::Sprite* Item::getPrizeSprite()
{
	return prizeSprite; // get sprite to runAction
}

ui::Text* Item::getPrizeText()
{
	return prizeText; // get prize text to runAction
}

std::string Item::getPrizeDescription()
{
	return prizeDescription; // get prize string to display at end (i.e. "You've won ______!!!")
}

int Item::getPrizeDropRate()
{
	return prizeDropRate; // prize drop rate out of 100%
}

void Item::savePrizeRotationDegrees(double degrees)
{
	prizeRotationDegrees = degrees; // save the angle that the prize sprite is rotated on wheel
}

double Item::getPrizeRotationDegrees()
{
	return prizeRotationDegrees; 
}

void Item::savePrizeSectorAngle(double degrees)
{
	prizeSectorAngle = degrees; // save the angle that the wheel spins to for this prize
}

double Item::getPrizeSectorAngle()
{
	return prizeSectorAngle;
}