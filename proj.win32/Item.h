#ifndef __ITEM_H__
#define __ITEM_H__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

USING_NS_CC_EXT;
//using namespace ui;

class Item
{
public:
	Item();

	Item(cocos2d::Sprite* name, std::string description, std::string imagePNG, int posX, int posY,
		double scaleBy, ui::Text* textName, std::string textDescription, int dropRatePercentage, cocos2d::Sprite* parent);

	cocos2d::Sprite* getPrizeSprite();
	ui::Text* getPrizeText();
	std::string getPrizeDescription();

	int getPrizeDropRate();

	void savePrizeRotationDegrees(double degrees);
	double getPrizeRotationDegrees();

	void savePrizeSectorAngle(double degrees);
	double getPrizeSectorAngle();

private:
	cocos2d::Sprite* prizeSprite;
	ui::Text* prizeText;
	std::string prizeDescription;

	int prizeDropRate;

	double prizeRotationDegrees;

	double prizeSectorAngle;
};

#endif __ITEM_H__