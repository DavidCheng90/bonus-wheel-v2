#include "Wheel.h"

Wheel::Wheel()
{
	prizeCount[8] = { };
	yourPrizeNum = 0;
	sectorAngle = 0.0;
	sectorAngleHalf = 0.0;
}

void Wheel::addPrizes(cocos2d::Sprite* parent)
{
	// add all prizes here
	Item prize1Heart(asset1Heart, "Life 30 mins.", "heart.png", 645, 830, 1.15, asset1HeartText, "30", 20, parent);
	prizeList.push_back(prize1Heart);
	Item prize2Brush(asset2Brush, "Brush 3x", "brush.png", 830, 650, 0.85, asset2BrushText, "x3", 10, parent);
	prizeList.push_back(prize2Brush);
	Item prize3Gem(asset3Gem, "Gems 35", "gem.png", 830, 390, 1.15, asset3GemText, "x35", 10, parent);
	prizeList.push_back(prize3Gem);
	Item prize4Hammer(asset4Hammer, "Hammer 3x", "hammer.png", 645, 210, 1.05, asset4HammerText, "x3", 10, parent);
	prizeList.push_back(prize4Hammer);
	Item prize5Coin(asset5Coin, "Coins 750", "coin.png", 390, 210, 0.95, asset5CoinText, "x750", 5, parent);
	prizeList.push_back(prize5Coin);
	Item prize6Brush(asset6Brush, "Brush 1x", "brush.png", 210, 390, 0.85, asset6BrushText, "x1", 20, parent);
	prizeList.push_back(prize6Brush);
	Item prize7Gem(asset7Gem, "Gems 75", "gem.png", 210, 650, 1.15, asset7GemText, "x75", 5, parent);
	prizeList.push_back(prize7Gem);
	Item prize8Hammer(asset8Hammer, "Hammer 1x", "hammer.png", 390, 830, 1.05, asset8HammerText, "x1", 20, parent);
	prizeList.push_back(prize8Hammer);

	setPrizeRotation(); // rotates all prize sprites correctly on wheel
	setPrizePool(); // create a pool of prizes based on each prize's drop rate (out of 100%)
}

void Wheel::setPrizeRotation()
{
	// rotates all prize sprites correctly on wheel- no need to edit
	sectorAngle = 360 / prizeList.size();
	sectorAngleHalf = sectorAngle / 2;
	for (int a = 0; a < int(prizeList.size()); ++a)
	{
		prizeList[a].savePrizeRotationDegrees(sectorAngleHalf + (sectorAngle * a));
		prizeList[a].getPrizeSprite()->setRotation(prizeList[a].getPrizeRotationDegrees());
		prizeList[a].savePrizeSectorAngle(360 - (sectorAngleHalf + sectorAngle * a));
	}
}

void Wheel::setPrizePool()
{
	// create a pool of prizes based on each prize's drop rate (out of 100%)
	for (int a = 1; a < int(prizeList.size() + 1); ++a)
		prizePool.insert(prizePool.end(), prizeList[a - 1].getPrizeDropRate(), a);
}

void Wheel::spinWheel()
{
	int randomNum = 0;
	std::vector<int>::iterator prizeSelector;

	// simulate 1000 spins
	for (int a = 0; a < 1000; ++a)
	{
		randomNum = cocos2d::RandomHelper::random_int(0, int(prizePool.size()) - 1);
		prizeSelector = prizePool.begin();
		advance(prizeSelector, randomNum);

		prizeCount[*prizeSelector - 1] += 1;

		if (a == 0) // use the first spin as the result for the wheel spin animation
			yourPrizeNum = *prizeSelector - 1;
	}
}

// ***** function for manually checking each sector of the wheel *****
void Wheel::testWheelSectorHere(int sectorNum)
{
	// if user tests for a sector, replace the result of the first random spin with this
	if (sectorNum >= 1 && sectorNum <= 8)
	{
		prizeCount[yourPrizeNum] -= 1;
		yourPrizeNum = sectorNum - 1;
		prizeCount[yourPrizeNum] += 1;
	}
}

void Wheel::wheelAnimation(cocos2d::Sprite* wheel, ui::Button* button, ui::Text* buttonText)
{
	CallFunc *playAnimation1 = CallFunc::create(std::bind(&Wheel::animationPart1, this, wheel, button, buttonText));

	auto spinWheelAnimation = Sequence::create(playAnimation1, NULL);

	wheel->runAction(spinWheelAnimation);
}

void Wheel::animationPart1(cocos2d::Sprite* wheel, ui::Button* button, ui::Text* buttonText)
{
	auto buttonGone = FadeOut::create(1);
	button->runAction(buttonGone->clone());
	buttonText->runAction(buttonGone->clone());

	auto spinWheelStage1 = RotateTo::create(0.1, 0);
	auto spinWheelStage2 = Repeat::create(RotateBy::create(1, 360), 3);
	auto spinWheelStage3 = RotateBy::create(1.3, 360);
	auto spinWheelStage4 = RotateBy::create(1.7, 360);
	auto spinWheelStage5 = RotateBy::create((prizeList[yourPrizeNum].getPrizeSectorAngle() / sectorAngleHalf) * 0.1375, prizeList[yourPrizeNum].getPrizeSectorAngle());
	auto delayAnimation1 = DelayTime::create(1.0);

	auto animationSequence1 = Sequence::create(spinWheelStage1, spinWheelStage2, spinWheelStage3, spinWheelStage4, spinWheelStage5, delayAnimation1, NULL);

	wheel->runAction(animationSequence1);
}