#ifndef __WHEEL_H__
#define __WHEEL_H__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "SimpleAudioEngine.h"
#include "Item.h"

USING_NS_CC;

USING_NS_CC_EXT;
//using namespace ui;

class Wheel
{
public:
	Wheel();

	void addPrizes(cocos2d::Sprite* parent);

	void spinWheel();

	// ***** function to manually check each sector of the wheel *****
	void testWheelSectorHere(int sectorNum);
	//****************************************************************

	void wheelAnimation(cocos2d::Sprite* wheel, ui::Button* button, ui::Text* buttonText);

	std::vector<Item> prizeList;
	int prizeCount[8];
	int yourPrizeNum;

private:
	// private methods
	void setPrizeRotation();
	void setPrizePool();

	void animationPart1(cocos2d::Sprite* wheel, ui::Button* button, ui::Text* buttonText);

	// private variables
	std::vector<int> prizePool;

	cocos2d::Sprite* asset1Heart;
	cocos2d::Sprite *asset2Brush;
	cocos2d::Sprite *asset3Gem;
	cocos2d::Sprite *asset4Hammer;
	cocos2d::Sprite *asset5Coin;
	cocos2d::Sprite *asset6Brush;
	cocos2d::Sprite *asset7Gem;
	cocos2d::Sprite *asset8Hammer;

	ui::Text *asset1HeartText;
	ui::Text *asset2BrushText;
	ui::Text *asset3GemText;
	ui::Text *asset4HammerText;
	ui::Text *asset5CoinText;
	ui::Text *asset6BrushText;
	ui::Text *asset7GemText;
	ui::Text *asset8HammerText;

	double sectorAngle;
	double sectorAngleHalf;
};

#endif __WHEEL_H__